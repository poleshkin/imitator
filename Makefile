#
#	Makefile проекта 
#

PROJECT = imitator

#
#	Setting
#

CFLAGS = -Wall -pedantic -g -std=c99 -funsigned-bitfields

CC = gcc $(CFLAGS)

SRC = ./src
DEST = ./appendix

# 
# 	Setting
# 
INCPATH = -I. -I./include -I/usr/local/include

# 
# 	Setting
# 
LIBPATH = -L ~/lib/usr/local/lib
LIBS = -lpthread -ljansson

# 
# 	Setting
# 

# 
# 	Переменная - все объектные файлы комипилируемого проекта
# 
OBJECTS = \
	$(DEST)/mb.o $(DEST)/bop1m.o $(DEST)/json.o $(DEST)/sem.o $(DEST)/opt.o $(DEST)/setopt.o $(DEST)/getopt.o

# 
# 	Setting
# 
imitator : obj main.o
	$(CC) $(INCPATH) -o $(DEST)/$(PROJECT) $(OBJECTS) $(DEST)/main.o $(LIBPATH) $(LIBS) 

# 	Объектные файлы исполнимого файла (не из состава бибилиотеки)
# 
obj	: \
	mb.o bop1m.o json.o sem.o opt.o setopt.o getopt.o




# 
# 	Объектные файлы компилируемой библиотеки
# 
clean	: 
	rm -f $(DEST)/*.o $(DEST)/*.so $(DEST)/$(PROJECT) $(DEST)/*.a $(DEST)/shm

mb.o : ./src/mb.c
	$(CC) -D_dEBUG_ -D_SVID_SOURCE $(INCPATH) -c -o $(DEST)/mb.o ./src/mb.c 

bop1m.o : ./src/bop1m.c
	$(CC) -D_dEBUG_ $(INCPATH) -c -o $(DEST)/bop1m.o ./src/bop1m.c 

json.o : ./src/json.c
	$(CC) -D_dEBUG_ -D_SVID_SOURCE -D_USER_OPTION_ $(INCPATH) -c -o $(DEST)/json.o ./src/json.c 

sem.o : ./src/sem.c
	$(CC) -D_dEBUG_ -D_SVID_SOURCE $(INCPATH) -c -o $(DEST)/sem.o ./src/sem.c

# 
# 	Модуль GETOPT
# 
setopt.o	: ./getopt/setopt.c ./getopt/setopt.h
	$(CC) -D_eXAMPLE_ -D_USER_OPTION_ $(INCPATH) -c -o $(DEST)/setopt.o ./getopt/setopt.c 

getopt.o	: ./getopt/getopt.c ./getopt/getopt.h
	$(CC) -D_dEBUG_ -D_eXAMPLE_ -D_USER_OPTION_ $(INCPATH) -c -o $(DEST)/getopt.o ./getopt/getopt.c 

opt.o	: ./getopt/opt.c ./getopt/opt.h
	$(CC) -D_eXAMPLE_ -D_USER_OPTION_ $(INCPATH) -c -o $(DEST)/opt.o ./getopt/opt.c 
 
# 
# 	
# 
 
main.o	: main.c 
	$(CC) -D_USER_OPTION_ $(INCPATH) -c -o $(DEST)/main.o main.c

 

