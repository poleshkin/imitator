#ifndef _GET_OPTION_H
#define _GET_OPTION_H

#include "getopt/option.h"
#include "getopt/setopt.h"

/*
 *	Macros difinition
 */

#define _GETOPTLN(opt, n)	( (char *) (opt[n].lname) )
#define _GETOPTSN(opt, n)	( (char *) (opt[n].sname) )

#define _GETOPTI(opt, n)	( *((int *) opt[n].value) )
#define _GETOPTL(opt, n)	( *((long *) opt[n].value) )
#define _GETOPTF(opt, n)	( *((float *) opt[n].value) )
#define _GETOPTS(opt, n)	( (char *) (opt[n].value) )

#define _ISGETOPTS(opt, n)	( *(_GETOPTS(opt, n)) != '\0' )

#define _GETOPT_FLAG_		1
#define _GETOPT_MSG_		2
#define _GETOPT_NUM_		3

/*
 *	Type declaration
 */

enum {
	GETOPT_FLAG = _GETOPT_FLAG_,
	GETOPT_MSG  = _GETOPT_MSG_,
	GETOPT_NUM
};

enum { 
	OPT_FAILURE = (-1), 
	OPT_SUCCESS = 0 
};

/*
 *	Data declaration
 */



/*
 *	Function declaration
 */

#ifdef __cplusplus
extern "C" {
#endif

int getoption(struct _option_ *, int, char **);

#if defined (_DEBUG_)
void oprint(struct _option_ *);
#endif

#ifdef __cplusplus
}
#endif

#endif /* _GET_OPTION_H */
