
#include <stdio.h>

#include "opt.h"
#include "getopt/setopt.h"


/*
 *	Data definition:
 */

static char 	*help[] = {
	"Usage: Imitator [OPTION1] [PATH] [OPTION2] [PATH]\n\n",
	"-h\t--help         display this help and exit\n",

#if defined ( _USER_OPTION_ )
	"-c\t--conf         open the configuration file\n",
	"-d\t--d         open the fifo file\n",
#endif
	NULL };

#if defined ( _USER_OPTION_ )
char		conf[128];
char		fifo[128];
#endif

struct _option_	option[] = {
	{   1, "-h", "--help",	help,	GETOPT_FLAG,	(setopt_t) getmsg 	},

#if defined ( _USER_OPTION_ )
	{	2, "-c", "--conf", conf,	GETOPT_MSG,		(setopt_t) setstr 	},
	{	3, "-d", "--dev", fifo,	GETOPT_MSG,		(setopt_t) setstr	},		

#endif
	{ 0, NULL, NULL, NULL, 0, NULL }
};

/*
 *	Function(s) definition:
 */

/*---------------------------------------------------------------------*

Name		-

Usage		- 

Prototype in	-     

Description	-     

*---------------------------------------------------------------------*/
void puthelp(void)
{
	puthlp(help);
}

