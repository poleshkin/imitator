
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "getopt/option.h"

/*
 *	Data definition:
 */



/*
 *	Function(s) definition:
 */

/*---------------------------------------------------------------------*

Name		-

Usage		- 

Prototype in	-     

Description	-     

*---------------------------------------------------------------------*/
void puthlp(char **msg)
{
	while ( *msg ) {
		fprintf(stderr, "%s", *msg);
		msg++;
	}
	
	exit(0);
}
/*---------------------------------------------------------------------*

Name		-

Usage		- 

Prototype in	-     

Description	-     

*---------------------------------------------------------------------*/
int getmsg(struct _option_ *opt, char *s)
{
	puthlp((char **)opt->value);
	
	return 0;
}

/*---------------------------------------------------------------------*

Name		-

Usage		- 

Prototype in	-     

Description	-     

*---------------------------------------------------------------------*/
int setstr(struct _option_ *opt, char *s)
{
	int	rc = 0;
	
	strcpy((char *)opt->value, s);

	return rc;
}
