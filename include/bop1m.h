#ifndef _bop1m_H
#define _bop1m_H

#include "pthread.h"
/*
 *	Macros difinition
 */
 #define	_REGS 	0x281

 /*
 *	Type declaration
 */
#pragma pack(1)

 struct _stval_ {				/*Содержит данные регистров статуса БД и его измеренного значения*/
 	short int 	status;
 	float 		val;
 	short int 	reserve[13];		/*Зарезервированные регистры*/
};


struct _actper_ {				/*Регистр с активностью (мл.байт) и периодом опроса (ст.байт) БД*/
	char period;
	char act; 
};	


struct _dibus_ {				/*DiBUS адрес БД (в формате а.b.c.channel)*/
	char b;
	char a;
	char chan;
	char c;
};


struct _props_ {							/*Характеристики БД*/
	struct 	_actper_ 	bd;
	struct 	_dibus_ 	adr;
	float 				pz;					/*Предупредительная уставка*/
	float				az;					/*Аварийная уставка*/
	float 				sk;					/*Уставка СК*/
	short int 			reserve[0x7];		/*Резервный регистр*/			
};

struct _date_ {					/*Регистр даты в формате дд.мм.гг*/
	unsigned int day:5;
	unsigned int mon:4;
	unsigned int year:7;
};


struct _ts_ {					/*Время в формате дата.часы.минуты.секунды.милисекунды*/
	struct _date_ 	date;
	char 			hour;
	char 			min;
	char 			sec;
	char 			msec;
};


struct _status_ {				/*Регистр статуса. Первый бит - состояние БД (0 - все ОК, 1 - есть отказы)*/
	unsigned int state:1;		/*Биты 8-11 - Статус БД1..БД4*/
	unsigned int reserve:7;
	unsigned int st_bd1:1;
	unsigned int st_bd2:1;
	unsigned int st_bd3:1;
	unsigned int st_bd4:1;
	unsigned int reserv2:4;
};


struct _flags_ {				/*Регистр флагов*/ 
	unsigned int pz1:1;			/*Биты 0-3 - Превышение предупредительного порога БД1..БД4 (0 - ОК, 1 - превышен)*/
	unsigned int pz2:1;			
	unsigned int pz3:1;
	unsigned int pz4:1;
	unsigned int az1:1;			/*Биты 4-7 - Превышение аварийного порога БД1..БД4 (0 - ОК, 1 - превышен)*/
	unsigned int az2:1;
	unsigned int az3:1;
	unsigned int az4:1;
	unsigned int reserve:8;
};


struct _data_ {
		struct _status_ 	status;					/*Регистр статус*/
		float		 		bd_val[4];				/*Измеренные значения БД1-БД4*/
		struct _flags_ 		flags;					/*Регистр флагов*/
		struct _ts_ 		time;					/*Регистры времени*/
		unsigned long		res;					/*Регистры ресурса*/
		short int 			reserve[0x101-15];		/*Зарезервированная область*/
		struct _stval_ 		bd[8];					/*Регистры статусов и измеренных значений БД1-БД8*/
		short int 			reserve2[0x201-0x181];	/*Зарезервированная область*/
		struct _props_ 		bds[8];					/*Регистры характеристик БД1-БД8*/
};


union _regs_ {
	short int 		area[0x281];
	struct _data_ 	data;
};

struct _range_ {
	float min;
	float max;
};

struct _bdlist_ {
	struct _bd_ *head;
	struct _bd_ *tail;
};

struct _bd_ {
	float			val;
	struct _range_	range;
	float			delta; 				
	short int 		*flags;
	float 			*pz;
	float 			*az;
	float			sk;
	short int 		i;
	struct _bd_ 	*next;
};

union _swap_ {
	float 			fl;
	char 			bytes[4];
};

struct _bop_ {
	union _regs_ 	*field;		//Регистровоей поле
	struct _bdlist_	bd;		    //Список БД, с которыми работает БОП
	pthread_t 		thread_id;	//ID потока
	unsigned char	bop_id;		//ID БОПа
	int 			segment_id; //ID сегмента памяти
	int 			sem_id;		//ID семафора
	struct _bop_ 	*next;		
} ;

#pragma pack()

extern union _regs_  regs;
extern struct _list_ list;


/*
 *	Data declaration
 */	

/*
 *	Function declaration
 */
#ifdef __cplusplus
extern "C" {
#endif

void* 	imit(void *parameters);
void 	init_regs(union _regs_ *a);
void 	make_math(struct _bd_ *p, struct _data_ *regs, int sem_id);
float	swap_fl(float val);


#ifdef __cplusplus
}
#endif 

#endif
