#ifndef _JSON_H
#define _JSON_H

#include "bop1m.h"
#include "jansson.h"

/*
 *	Macros difinition
 */
#define _SEG_SIZE	0X6400
#define _SHMKEY		0XBADFACE


/*
 *	Type declaration
 */

struct _boplist_ {
	struct _bop_ *head;
	struct _bop_ *tail;

};


/*
 *	Data declaration
 */	

/*
 *	Function declaration
 */

#ifdef __cplusplus
extern "C" {
#endif

int	 load_cfg(struct _boplist_ *list);
int parse_json_array(json_t *element, struct _boplist_ *list);
void parse_json_object(json_t *element, struct _bop_ *bop, int i);
void create_bop_node(struct _boplist_ *list,int i); 

#ifdef __cplusplus
}
#endif

#endif /* _NAME_H */
