#ifndef _MB_H
#define _MB_H



/*
 *	Macros difinition
 */
#define _CRC_SIZE		sizeof(short int)
#define _FIFO	 		"/dev/ttyMXUSB1"
#define _BYTECOUNT 		8
//#define _CRC_SIZE	sizeof(short int)



/*
 *	Type declaration
 */



/*
 *	Data declaration
 */	
#pragma pack(1)

struct _head_ {
	unsigned char id;
	unsigned char func;
};

struct _rpack_ {
	struct _head_	head;
	unsigned short 	adr;
	unsigned short 	num;
};



struct _apack03_ {
	struct _head_ 	head;
	unsigned char 	size;
	char 			data;
};

struct _apack10_ {
	struct _head_	head;
	unsigned short 	adr;
	unsigned short 	num;
};



#pragma pack()

/*
 *	Function declaration
 */

#ifdef __cplusplus
extern "C" {
#endif

int 			read_regs(struct _rpack_ *a, short int *regs, int fd, int sem_id);
int 			write_regs(struct _rpack_ *a, short int *regs, int fd, int sem_id);
void			swap(unsigned short *a);
unsigned short 	crc16(unsigned char *buffer, unsigned short buffer_length);
void 			set_flag(void);
void			sem_alloc();

#ifdef __cplusplus
}
#endif

#endif /* _NAME_H */
