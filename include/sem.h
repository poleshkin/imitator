#ifndef _SEMAPHORE_H
#define _SEMAPHORE_H



/*
 *	Macros difinition
 */



/*
 *	Type declaration
 */



/*
 *	Data declaration
 */	

/*
 *	Function declaration
 */

#ifdef __cplusplus
extern "C" {
#endif

int lock_sem(int sem_id);
int unlock_sem(int sem_id);

#ifdef __cplusplus
}
#endif

#endif /* _NAME_H */
