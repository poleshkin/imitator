#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/time.h>
#include <sys/types.h>
#include <json.h>
#include <mb.h>
#include <getopt/opt.h>

unsigned char buffer[255];


int main(int argc, char **argv)
{
	unsigned short int 		check;	//Для проверки контрольной суммы
	int 					fd,n;	//Файловый дескриптор и общая переменная n для вычислений 
	int 					rc=0;	
	struct _rpack_ 			*req=(struct _rpack_ *)buffer;
	struct timeval 			tv;
	struct _boplist_		bops;
	struct _bop_ 			*tmp;	//Копия головы списка БОПов для обхода списка
	fd_set 					rfds;

	if (getoption(option, argc, argv) != OPT_SUCCESS)
	{
		puthelp();
	}

	bops.head=NULL;     //Зануляем голову и хвост списка бопов
    bops.tail=NULL;
	
	fd=open(_GETOPTS(option, OPT_FIFO),O_RDWR);
	if (fd<=0)
	{		
		perror("Не удалось открыть файл устройства: \n");
		rc=-1;
	}
	else
	{	printf("Файл устройства открыт\n");
		rc=load_cfg(&bops);			//Загружаем json-файл в список. Передаем "голову" и "хвост" будущего списка	

		if (rc)
		{
			printf("Ошибка загрузки файла конфигурации\n");
			exit(-1);
		}
		else
		{
			while (1)
			{
				tv.tv_sec=0;
				tv.tv_usec=500000;
				FD_ZERO(&rfds);
				FD_SET(fd,&rfds);
				n=select(fd+1,&rfds,NULL,NULL,&tv);    //Сохраняем в n количество готовых для записи дескрипторов

				if (n>0)
				{
					n=read(fd,buffer,256);
					if (n>0)			
					{
						tmp=bops.head;
						while (tmp)
						{
							if (tmp->bop_id==req->head.id)
							{
								check=crc16(buffer,n-_CRC_SIZE); //Вычисляем crc для запроса
								swap(&req->num);
								swap(&req->adr);
								swap((unsigned short *)((char *)req+n-sizeof(short int)));

								printf("Запрос\nID %#x\t FUNC %#x\t Adr %#x\t Size %#x\t CRC %#x\n",req->head.id,req->head.func,req->adr,req->num,*((unsigned short *)((char *)req+n-sizeof(short int))));
			
								if (*((unsigned short *)((char *)req+n-sizeof(short int)))==check) //Если контрольные суммы совпадают, то формируем ответ
								{
									switch (req->head.func)
									{
										case 0x3:
											read_regs(req,tmp->field->area,fd, tmp->sem_id);  														
											break;
										case 0x10:
											write_regs(req,tmp->field->area,fd, tmp->sem_id);
											break;
									}
								}
								else
								{
									printf("Контрольные суммы не совпали %x\t %x\n", check, *((unsigned short *)((char *)req+n-sizeof(short int))));									
								}
								break;
							}
							else
							{
								tmp=tmp->next;
							}	
						}		
					}
				}
			}	
		}
	}

	return rc;
}
