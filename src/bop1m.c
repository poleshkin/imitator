#include <stdio.h>
#include <unistd.h>
#include "json.h"
#include "bop1m.h"
#include "mb.h"
#include "sem.h"

#define _DELTA 	4
#define _dEBUG


/*
 *	Data definition:
 */


 /*
 *	Function(s) definition:
 */
 /*---------------------------------------------------------------------*

Name		-init_regs

Usage		- read_regs

Prototype in	-     mb.c

Description	- Инициализация регистров
*---------------------------------------------------------------------*/
void init_regs(union _regs_ *a)
{
	int i;

	for (i=1;i<_REGS;i++)
		{
			a->area[i]=0x0;
		}


}
 /*---------------------------------------------------------------------*

Name		- set_flags

Usage		- main.c

Prototype in	-     bop1m.c

Description	- Увеличивает значение полей float на 0.1. Изменяет флаги блоков детектирования в зависимости
от измеренного значения
*---------------------------------------------------------------------*/

void* imit(void *parameters)
{
	struct _bop_  *bop=(struct _bop_ *)parameters;
	struct _bd_   *p;

	while(1)
	{	
		p=bop->bd.head;
		printf("BOP ID = %d\n", bop->bop_id);
			
		while (p)
		{
            printf("BD%d = %.3f\n PZ = %.3f AZ = %.3f\n", p->i+1, p->val, swap_fl(*p->pz), swap_fl(*p->az));
            make_math(p, &bop->field->data, bop->sem_id);

			if(p->val >= swap_fl(*p->az))
			{
				*p->flags|= (1 << (_DELTA+p->i));
				*p->flags|= (1 << p->i);		
			}
			else if(p->val >= swap_fl(*p->pz))
			{
				if(p->val < swap_fl(*p->az))
				{
					*p->flags&= ~(1 << (_DELTA+p->i));
				}
				*p->flags|= (1 << p->i);
			}
			else
			{
				*p->flags&= ~(1 << (_DELTA+p->i));
				*p->flags&= ~(1 << p->i);
			}

			p=p->next;
		}
		printf("\n\n");
			sleep(1);
	}

	return NULL;
}

 /*---------------------------------------------------------------------*

Name		- regs_math

Usage		- set_flegs

Prototype in	- bop1m.c

Description	- Увеличивает значение полей float на значение delta. Изменяет флаги блоков детектирования в зависимости
от измеренного значения
*---------------------------------------------------------------------*/

void make_math (struct _bd_ *p, struct _data_ *regs, int sem_id)
{
		
	
	if (p->val <= p->range.min || p->val >= p->range.max)	//Реализация "пилы"
	{
		p->delta=(-1)*p->delta;
	}
	
	
	p->val+=p->delta;

					
	if (p->val < p->range.min)		//Если новое значение вышло за границы диапазона, 
	{
		p->val=p->range.min;	//то присваиваем значение границы
	}
	else if (p->val > p->range.max)
	{
		p->val=p->range.max;
	}	

	if (lock_sem(sem_id) == 0)
	{
		regs->bd_val[p->i]=swap_fl(p->val);
		regs->bd[p->i].val=swap_fl(p->val);
		if (unlock_sem(sem_id) != 0)
		{
			perror ( "semop unlock_sem");
			exit(-1);
		}
	}
	else
	{
		perror ( "semop lock_sem");
		exit(-1);
	}



}

float swap_fl (float val)
{
	union _swap_ swap;
	char tmp;

	swap.fl=val;

	tmp=swap.bytes[3];
	swap.bytes[3]=swap.bytes[0];
	swap.bytes[0]=tmp;
	tmp=swap.bytes[2];
	swap.bytes[2]=swap.bytes[1];
	swap.bytes[1]=tmp;

	return swap.fl;
}
