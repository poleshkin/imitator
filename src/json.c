
#include "json.h"
#include "getopt/opt.h"
#include <sys/shm.h>
#include <sys/stat.h>
#include <sys/sem.h>


/*
 *	Data definition:
 */

/*
 *	Function(s) definition:
 */

/*---------------------------------------------------------------------*

Name		- load_cfg

Usage		- main.c

Prototype in	- json.c   

Description	- Загружает конфиг. файл типа json     

*---------------------------------------------------------------------*/

int load_cfg(struct _boplist_ *list)
{
    int                 rc=0;
    json_error_t        error;
    json_t              *root;
    
	root=json_load_file(_GETOPTS(option, OPT_CONF),JSON_ENCODE_ANY,&error);

	if (root) 
	{
		if (json_typeof(root)==JSON_ARRAY)
		{
            rc = parse_json_array(root, list);
            json_decref(root);
        }
        else
        {
            printf("Массив БОП-1М не найден\n");
            rc=-1;
        }  
    }
    else
    {
    	printf("Не удалось загрузить файл конфигурации: %s\n", error.text);
        rc=-1;
    }

    return rc;
}
/*---------------------------------------------------------------------*

Name		- load_cfg

Usage		- main

Prototype in	- json.c   

Description	- Разбирает json массив на объекты    

*---------------------------------------------------------------------*/
int parse_json_array(json_t *element, struct _boplist_ *bops) 
{
    int                 rc=0;
    int                 id;
    size_t              i,j,bd_s;
    size_t              size = json_array_size(element);
    json_t              *tmp,*bd;

    for (i = 0; i < size; i++)                  //Перебираем массив БОПОВ в config.json
    {
        tmp=json_array_get(element, i);         //Получаем БОП из массива
        if (json_typeof(tmp) == JSON_OBJECT)
        {
            id=json_integer_value(json_object_get(tmp,"bop_id"));
            create_bop_node(bops,id);               //Создаем очередной боп в списке
            bd=json_object_get(tmp,"BDs");
            if (json_typeof(bd) == JSON_ARRAY)
            {
                bd_s=json_array_size(bd);
                for (j = 0; j < bd_s; j++)         //Перебираем массив БД для каждого БОПА
                {
                    parse_json_object(json_array_get(bd, j), bops->tail, j); //Парсим данные в БД
                }
                pthread_create(&bops->tail->thread_id,NULL,&imit,bops->tail);
            }
            else
            {
                printf("Неверный JSON тип данных - список блоков детектирования не является массивом. Тип BDs = %d\n", json_typeof(bd));
                rc=-1;
            }    

        }
        else
        {
             printf("Неверный JSON тип данных - устройство БОП-1М за индексом %d не является объектом", i);
             rc = -1;
        }
     
    }

    return rc;
}
/*---------------------------------------------------------------------*

Name		- parse_json_object

Usage		- load_cfg

Prototype in	- json.c   

Description	- Разбирает json объект, создает ноду и инициализирует ее парами объекта, подвязывает ноду к списку    

*---------------------------------------------------------------------*/
void parse_json_object(json_t *element, struct _bop_ *bop, int i) 
{
    struct _bd_ *new;

    new=(struct _bd_ *)malloc(sizeof(struct _bd_));

    new->val =       json_real_value(json_object_get(element,"val"));
    new->flags =     &bop->field->area[9];
    new->i =         (short int)json_integer_value(json_object_get(element,"index"));
    new->range.min = json_real_value(json_object_get(element,"min"));
    new->range.max = json_real_value(json_object_get(element,"max"));
    new->delta =     json_real_value(json_object_get(element,"step"));
    new->az =        (float *)&bop->field->data.bds[i].az; 
    new->pz =        (float *)&bop->field->data.bds[i].pz;
    new->sk =        json_real_value(json_object_get(element,"sk"));
    new->next =      NULL;


    bop->field->data.bds[i].az=swap_fl(json_real_value(json_object_get(element,"az")));         //Кладем уставки БД в соответствующие регистры БОПа
    bop->field->data.bds[i].pz=swap_fl(json_real_value(json_object_get(element,"pz")));
    bop->field->data.bds[i].sk=swap_fl(new->sk);


    if (!bop->bd.head)              //Подвязываем БД к списку блоков для данного БОПа
    {
        bop->bd.head=new;
        bop->bd.tail=new;
    }
    else
    {
        bop->bd.tail->next = new;
        bop->bd.tail = new; 
    }
}
/*---------------------------------------------------------------------*

Name        - create_bop_node

Usage       - parse_json_array

Prototype in    - json.c  

Description - Создает БОП и подвязывает его к списку    

*---------------------------------------------------------------------*/
void create_bop_node(struct _boplist_ *list,int id)
{
    struct _bop_    *new;

    new = malloc(sizeof(struct _bop_));

    new->segment_id = shmget(_SHMKEY + id, sizeof(union _regs_),IPC_CREAT | IPC_EXCL | 0777);
    if (new->segment_id == -1)
    {
        new->segment_id = shmget(_SHMKEY + id, sizeof(union _regs_),0);
        if (new->segment_id == -1)
        {
            perror("Не удалось выделить сегмент памяти под устройство\n");
            printf("BOP ID = %d\n",id);
            exit(-1);
        }
    }
    new->field=(union _regs_ *)shmat(new->segment_id, 0, 0);

    new->sem_id=semget(_SHMKEY + id, 1, IPC_CREAT | 0777);
    if (new->sem_id==-1)
    {
        perror("Не удалось выделить семафор под устройство\n");
        printf("BOP ID = %d\n",id);
        exit(-1);
    }
    semctl(new->sem_id, 0, SETVAL, 1);

    new->bd.head=NULL;        //Зануляем голову и хвост списка блоков детектирования, с которыми будет работать 
    new->bd.tail=NULL;        //данный боп.

    new->bop_id=(char)id;

    init_regs(new->field);

    if (!list->head)
    {
        list->head=new;
        list->tail=new;
    }
    else
    {
        list->tail->next = new;
        list->tail = new; 
    }
    new->next=NULL;
}
