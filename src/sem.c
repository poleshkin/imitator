#include <sem.h>
#include <sys/sem.h>

/*
 *  Function(s) definition:
 */

/*---------------------------------------------------------------------*

Name        - lock_sem  

Usage       - shm.c, mb.c

Prototype in    -     sem.c

Description -     Переключает семафор в состояние "Занято"

*---------------------------------------------------------------------*/
int lock_sem(int sem_id)
{
    struct sembuf operations[1];

    operations[0].sem_num=0;
    operations[0].sem_op=-1;
    operations[0].sem_flg=0;

    return semop(sem_id, operations, 1);
}
/*
 *  Function(s) definition:
 */

/*---------------------------------------------------------------------*

Name        - unlock_sem

Usage       - shm.c, mb.c

Prototype in    -   sem.c  

Description -     Переключает семафор в состояние "Свободно"

*---------------------------------------------------------------------*/
int unlock_sem(int sem_id)
{
    struct sembuf operations[1];

    operations[0].sem_num=0;
    operations[0].sem_op=1;
    operations[0].sem_flg=0;

    return semop(sem_id, operations, 1);
}
